# Trainz

Zarządzanie rozkładami jazdy pociągów. Program pozwala na zbudowanie rozkładu
oraz na wyszukiwanie odpowiednich połączeń pomiędzy stacjami.

## Budowanie

    ./Setup.lhs configure
    ./Setup.lhs build

## Uruchamianie

    ./Trainz
