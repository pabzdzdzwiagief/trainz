module Main where

import Types(Train, Station)
import Functionality
import Inout

-- Funkcja główna programu, inicjuje wyświetlanie menu.
main :: IO ()
main = do
     putStrLn "Wczytywanie pociągów i stacji z plików"
     trainz <- readDataFile "trainz.txt"
     stationz <- readDataFile "stationz.txt"
     trainsApp trainz stationz

-- Wyświetla menu, pobiera komendę i przechodzi jej do obsługi, aktualizuje
-- stan, po czym przechodzi do początku.
trainsApp :: [Train] -> [Station] -> IO ()
trainsApp trains stations = do
        choice <- showMenu
        let operation =  case choice of
                "1"  -> newStation
                "2"  -> modStation
                "3"  -> delStation
                "4"  -> showStations
                "5"  -> newTrain
                "6"  -> modTrain
                "7"  -> delTrain
                "8"  -> showTrains
                "9"  -> findConnections
                "10" -> generateTimetable
                "11" -> exit
                _    -> complain
        (newTrains, newStations) <- operation trains stations
        trainsApp newTrains newStations

-- Wyświetla menu i pobiera polecenie użytkownika.
showMenu :: IO String
showMenu = do
        putStrLn "Menu"
        putStrLn "1.  Dodaj nową stację"
        putStrLn "2.  Zmodyfikuj stację"
        putStrLn "3.  Usuń stację"
        putStrLn "4.  Pokaż stacje"
        putStrLn "5.  Wprowadź nowy pociąg"
        putStrLn "6.  Zmodyfikuj pociąg"
        putStrLn "7.  Usuń pociąg"
        putStrLn "8.  Pokaż pociągi"
        putStrLn "9.  Znajdź połączenia"
        putStrLn "10. Wygeneruj rozkład jazdy pociągów dla stacji"
        putStrLn "11. Zakończ"
        getLine
