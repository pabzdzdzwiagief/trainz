module Types where

import Data.List

--------------------------------------- Day ------------------------------------

-- Typ dnia tygodnia.
data Day = Po | Wt | Sr | Cz | Pt | So | Ni
    deriving (Show, Eq, Ord, Enum, Read)

nameOfDay :: Day -> String
nameOfDay d = case d of
                  Po -> "Po"
                  Wt -> "Wt"
                  Sr -> "Sr"
                  Cz -> "Cz"
                  Pt -> "Pt"
                  So -> "So"
                  Ni -> "Ni"

dayFromString :: String -> Day
dayFromString d = case d of
                      "Po" -> Po
                      "Wt" -> Wt
                      "Sr" -> Sr
                      "Cz" -> Cz
                      "Pt" -> Pt
                      "So" -> So
                      "Ni" -> Ni
                      _ -> error "Bad day name"

-- Zamienia listy dni na napis, w którym dni oddzielone są spacją
strDays :: [Day] -> String
strDays days = unwords $ map nameOfDay days

------------------------------------- Station ----------------------------------

-- Typ definiujący stację:
-- stName   - nazwa stacji
-- stTrains - lista nazw pociągów przejeżdżających przez stację
data Station = Station {
    stName :: String,
    stTrains :: [String]
} deriving (Show, Read, Eq, Ord)

-- Sprawdza, czy stacja o danej nazwie znajduje się na liście stacji.
hasStation :: String -> [Station] -> Bool
hasStation name stations = any (\(Station statName _) -> name == statName) stations

-- Próbuje znaleźć stację o danej nazwie na liście stacji. Jeśli jej nie ma,
-- zwraca Nothing, w p.p. zwraca Just stacja.
findStation :: String -> [Station] -> Maybe Station
findStation name stations = find (\(Station statName _) -> name == statName) stations

-- Jeśli pociąg przejeżdża przez stację, zwraca stację z listą nazw pociągów
-- powiększoną o nazwę tego pociągu, w p.p. zwraca stację wejściową.
addTrainIfRelated :: Train -> Station -> Station
addTrainIfRelated added (Station name trains) | hasStationOnRoute added name = Station name (trName added : trains)
                                              | otherwise                    = Station name trains

-- Usuwa nazwę pociągu z listy nazw pociągów danej stacji i zwraca taką stację.
removeTrain :: String -> Station -> Station
removeTrain removed (Station name trains) = Station name withoutRemoved
        where withoutRemoved = filter (\x -> x /= removed) trains

------------------------------------ Train -------------------------------------

-- Typ definiujący pociąg:
-- trName  - nazwa pociągu
-- trDays  - lista dni kursowania pociągu
-- trRoute - trasa pociągu
data Train = Train {
    trName :: String,
    trDays :: [Day],
    trRoute :: Route
} deriving (Show, Read, Eq, Ord)

-- Sprawdza, czy pociąg o danej nazwie znajduje się na liście pociągów.
hasTrain :: String -> [Train] -> Bool
hasTrain name trains = any (\(Train trainName _ _) -> name == trainName) trains

-- Sprawdza, czy stacja o danej nazwie znajduje się na trasie pociągu.
hasStationOnRoute :: Train -> String -> Bool
hasStationOnRoute train name = any (\x -> name == rpStation x) $ (roStations . trRoute) train

-- Zwraca pociąg, na którego trasie stacji o starej nazwie nadawana jest nowa nazwa.
renameStationForTrain :: String -> String -> Train -> Train
renameStationForTrain oldName newName (Train name days route) = Train name days newRoute
       where newRoute = renameStationOnRoute oldName newName route

-- Zwraca pociąg, na którego trasie usuwana jest stacja o danej nazwie.
removeStationForTrain :: String ->  Train -> Train
removeStationForTrain removedName (Train name days route) = Train name days newRoute
       where newRoute = removeStationOnRoute removedName route

------------------------------------ Route -------------------------------------

-- Typ definiujący trasę pociągu:
-- roStations - lista punktów trasy
data Route = Route {
    roStations :: [RoutePoint]
} deriving (Show, Read, Eq, Ord)


-- Jeśli trasa jest pusta zwraca Nothing, w p.p. zwraca nazwę pierwszej stacji
-- na trasie.
routeStart :: Route -> Maybe String
routeStart (Route []) = Nothing
routeStart (Route (RoutePoint name _ _:_)) = Just name

-- Jeśli trasa jest pusta zwraca Nothing, w p.p. zwraca nazwę ostatniej
-- stacji na trasie.
routeEnd :: Route -> Maybe String
routeEnd (Route []) = Nothing
routeEnd (Route points) = let RoutePoint name _ _ = last points in Just name

-- Zwraca trasę, na której stacji o starej nazwie nadawana jest nowa nazwa.
renameStationOnRoute :: String -> String -> Route -> Route
renameStationOnRoute oldName newName (Route points) = Route (map rename points)
        where rename x | oldName == rpStation x = renameRoutePointStation newName x
                       | otherwise              = x

-- Zwraca trasę, na której usuwana jest stacja o danej nazwie.
removeStationOnRoute :: String -> Route -> Route
removeStationOnRoute removedName (Route points) = Route newPoints
        where newPoints = filter (\x -> removedName /= rpStation x) points

--------------------------------- RoutePoint -----------------------------------

-- Typ definiujący punkt trasy pociągu:
-- rpStation   - nazwa stacji
-- rpArrival   - czas przyjazdu na stację (Nothing dla stacji początkowych na trasie)
-- rpDeparture - czas przyjazdu na stację (Nothing dla stacji końcowych na trasie)
data RoutePoint = RoutePoint {
    rpStation :: String,
    rpArrival :: Maybe Time,
    rpDeparture :: Maybe Time
} deriving (Show, Read, Eq, Ord)

-- Sprawdza, czy czas przyjazdu na dany punkt trasy jest wcześniejszy niż czas odjazdu.
validRoutePoint :: RoutePoint -> Bool
validRoutePoint (RoutePoint _ _ Nothing) = True
validRoutePoint (RoutePoint _ arrTime depTime) = arrTime < depTime

-- Sprawdza, czy lista punktów trasy jest ułożona poprawnie.
validRoutePoints :: [RoutePoint] -> Bool
validRoutePoints rps = areOrdered rps

-- Sprawdza, czy lista punktów trasy jest uporządkowana względem następowania
-- po sobie czasów przyjazdu i odjazdu.
areOrdered :: [RoutePoint] -> Bool
areOrdered []  = True
areOrdered rps = foldl (&&) True (zipWith isBefore (init rps) (tail rps))

-- True jeśli pierwszy punkt występuje na trasie wcześniej od drugiego.
isBefore :: RoutePoint -> RoutePoint -> Bool
isBefore (RoutePoint _ _ d1) (RoutePoint _ a2 _) = d1 < a2

-- Sprawdza, czy czas przyjazdu/odjazdu nie jest sprzeczny z resztą
validWithOtherRoutePoints :: [RoutePoint] -> Maybe Time -> Bool
validWithOtherRoutePoints rps x = null (filter(\y -> betweenArrivalAndDeparture y x) rps)

-- Sprawdza, czy w zadanym momencie pociąg stoi na stacji.
betweenArrivalAndDeparture :: RoutePoint -> Maybe Time -> Bool
betweenArrivalAndDeparture (RoutePoint _ _ _) Nothing = False
betweenArrivalAndDeparture (RoutePoint _ Nothing depTime) x = x < depTime
betweenArrivalAndDeparture (RoutePoint _ arrTime Nothing) x = x > arrTime
betweenArrivalAndDeparture (RoutePoint _ arrTime depTime) x = (x > arrTime) && (x < depTime)

-- Zwraca punkt trasy ze zmienioną nazwą stacji.
renameRoutePointStation :: String -> RoutePoint -> RoutePoint
renameRoutePointStation newName (RoutePoint _ arrTime depTime) = RoutePoint newName arrTime depTime

------------------------------------ Time --------------------------------------

-- Typ definiujący czas:
-- tHour   - godzina
-- tMinute - minuta
data Time = Time {
    tHour :: Int,
    tMinute :: Int
} deriving (Show, Read, Eq, Ord)

-- Zwraca czas w postaci napisu w formacie GG:MM.
showTime :: Time -> String
showTime (Time h m) = showNumber h ++ ":" ++ showNumber m
    where
        showNumber x = if x < 10 then "0" ++ show x
                       else show x

-- Zwraca czas będący różnicą czasu między pierwszym a drugim czasem.
timeDiff :: Time -> Time -> Time
timeDiff (Time h1 m1) (Time h2 m2) = if m1 < m2 then (Time (h1 - 1 - h2) (m1 + 60 - 1))
                                     else (Time (h1 - h2) (m1 - m2))

---------------------------------- TimeTable -----------------------------------

-- Typ definiujący rozkład jazdy dla jednej stacji:
-- ttName       - nazwa stacji
-- ttArrivals   - lista przyjazdów (nazwa pociągu, stacja początkowa,
--                stacja końcowa, czas przyjazdu, dni kursowania)
-- ttDepartures - lista odjazdów (nazwa pociągu, stacja początkowa,
--                stacja końcowa, czas odjazdu, dni kursowania)
data TimeTable = TimeTable {
    ttName :: String,
    ttArrivals :: [ (String, String, String, Time, [Day]) ],
    ttDepartures :: [ (String, String, String, Time, [Day]) ]
} deriving (Show, Read, Eq, Ord)

-- Zamienia rozkład jazdy na napis.
formatTimeTable :: TimeTable -> String
formatTimeTable timeTable = line ++ header ++ line ++ arrivals ++ departures ++ line
    where
        line = "*************************************************************\n"
        header = "Rozkład przyjazdów i odjazdów dla stacji " ++ ttName timeTable ++ "\n"
        -- Napis reprezentujący listę przyjazdów.
        arrivals = "Przyjazdy\n" ++ foldr (\arr acc -> infoToStr arr ++ acc) "" (ttArrivals timeTable)
        -- Napis reprezentujący listę odjazdów.
        departures = "Odjazdy\n" ++ foldr (\arr acc -> infoToStr arr ++ acc) "" (ttDepartures timeTable)
        -- Zwraca element rozkładu jazdy w postaci napisu.
        infoToStr (tname, from, to, time, days) = showTime time ++ "  " ++ from ++ "-" ++ to ++ ", pociąg " ++ tname ++ ", dni: " ++ strDays days ++ "\n"

-- Zwraca rozkład jazdy dla danej stacji uwzględniający pociągi znajdujące
-- się na przekazanej liście, przyjazdy i odjazdy posortowane są na
-- podstawie czasu odpowiednio przyjazdu i odjazdu pociągu.
doTimeTable :: Station -> [Train] -> TimeTable
doTimeTable (Station name tnames) trains = TimeTable name sortedArrivals sortedDepartures
    where
        arrivingTrains = filter (\(Train tname _ _) -> tname `elem` tnames) trains
        -- Zwraca listę przyjazdów do danej stacji
        arrivals :: [ (String, String, String, Time, [Day]) ]
        arrivals = concat $ map (getArrivals name) arrivingTrains
        sortedArrivals = sortBy sortTimeTableEntry arrivals
        -- Zwraca listę odjazdów z danej stacji
        departures :: [ (String, String, String, Time, [Day]) ]
        departures = concat $ map (getDepartures name) arrivingTrains
        sortedDepartures = sortBy sortTimeTableEntry departures

-- Zwraca porządek sortowania, który określony jest na podstawie czasu,
-- między dwoma elementami rozkładu jazdy.
sortTimeTableEntry :: (String, String, String, Time, [Day]) -> (String, String, String, Time, [Day]) -> Ordering
sortTimeTableEntry (_, _, _, t1, _) (_, _, _, t2, _) = compare t1 t2

-- Zwraca elementy rozkładu jazdy reprezentujące przyjazdy pociągu na stację.
getArrivals :: String -> Train -> [ (String, String, String, Time, [Day]) ]
getArrivals statName (Train tname days route) = map arrivalInfo rpoints
    where
        -- Lista punktów trasy pociągu takich, że przyjeżdża na daną stację.
        rpoints = filter ((statName ==).rpStation) (tail $ roStations route)
        Just from = routeStart route
        Just to = routeEnd route
        -- Zamienia punkt trasy na element rozkładu jazdy.
        arrivalInfo rp = case rp of
                             RoutePoint _ (Just t1) _ -> (tname, from, to, t1, days)
                             RoutePoint _ Nothing _ -> error "This RoutePoint should not have empty arrival time"

-- Zwraca elementy rozkładu jazdy reprezentujące odjazdy pociągu ze stacji.
getDepartures :: String -> Train -> [ (String, String, String, Time, [Day]) ]
getDepartures statName (Train tname days route ) = map departureInfo rpoints
    where
        -- Lista punktów trasy pociągu takich, że odjeżdża z danej stacji.
        rpoints = filter ((statName ==).rpStation) (init $ roStations route)
        Just from = routeStart route
        Just to = routeEnd route
        -- Zamienia punkt trasy na element rozkładu jazdy.
        departureInfo rp = case rp of
                               RoutePoint _ _ (Just t2) -> (tname, from, to, t2, days)
                               RoutePoint _ _ Nothing -> error "This RoutePoint should not have empty departure time"

---------------------------------- Connection ----------------------------------

-- Typ definiujący połączenie - trasę między dwiema stacjami, którą
-- można pokonać z przesiadkami w określone dni:
-- coSections - elementy trasy
-- coDays     - dni, w które dane połączenie jest możliwe, tzn. kursują
--              wszystkie potrzebne pociągi
data Connection = Connection {
    coSections :: [Section],
    coDays :: [Day]
} deriving (Show, Read)

-- Zamienia połączenie na napis.
formatConnection :: Connection -> String
formatConnection (Connection sections days) = header ++ strSections
    where
        header = "Połączenie dostępne w dniach: " ++ strDays days ++ "\n"
        strSections = foldr (\sec acc -> formatSection sec ++ acc) "" sections

-- Zamienia listę połączeń na napis razem z nagłówkiem zawierającym
-- informacje o początku i końcu trasy oraz maksymalnej liczbie przesiadek.
formatConnections :: [Connection] -> String -> String -> Int -> String
formatConnections connections from to n = line ++ header ++ line ++ strConnections
    where
        header = "Połączenia dla relacji " ++ from ++ "-" ++ to ++ " z maksymalną liczbą przesiadek równą " ++ (show n) ++ "\n"
        line = "*************************************************************\n"
        strConnections = foldr (\conn acc -> formatConnection conn ++ line ++ acc) "" connections

-- Zwraca czas odjazdu z pierwszej stacji na danym połączeniu.
connStart :: Connection -> Time
connStart = seBegin . head . coSections

-- Zwraca czas jazdy danym połączeniem (od czasu odjazdu z pierwszej stacji
-- do czasu przyjazdu na ostatnią stację).
connLength :: Connection -> Time
connLength (Connection sections _) = timeDiff (seEnd $ last $ sections) (seBegin $ head $ sections)

-- Zwraca porządek sortowania między dwoma połączeniami, który określony
-- jest na podstawie czasu rozpoczęcia jazdy i czasu jazdy.
compareConnections :: Connection -> Connection -> Ordering
compareConnections conn1 conn2 = let lengthComparison = compare (connStart conn1) (connStart conn2)
                                 in if lengthComparison == EQ then
                                        compare (connLength conn1) (connLength conn2)
                                    else lengthComparison

----------------------------------- Section ------------------------------------

-- Typ definiujący sekcję - element trasy połączenia, czyli przejazd między
-- dwiema kolejnymi stacjami:
-- seTrain - nazwa pociągu
-- seFrom  - nazwa stacji, z której pociąg odjeżdża
-- seTo    - nazwa stacji, na którą pociąg dojeżdża
-- seBegin - czas odjazdu z pierwszej stacji
-- seEnd   - czas dojazdu na drugą stację
data Section = Section {
    seTrain :: String,
    seFrom :: String,
    seTo :: String,
    seBegin :: Time,
    seEnd :: Time
} deriving (Show, Read)

-- Zamienia sekcję na napis.
formatSection :: Section -> String
formatSection (Section train from to dep arr) = showTime dep ++ " " ++ from ++ " -- " ++ showTime arr ++ " " ++ to ++ ", pociąg " ++ train ++ "\n"

-- Zwraca listę połączeń od pierwszą do drugiej stacji korzystając
-- z pociągów dostępnych na danej liście z maksymalną liczbą przesiadek
-- określoną w ostatnim argumencie. Połączenia są posortowane najpierw
-- względem czasu odjazdu, a następnie czasu podróży.
findConnections :: String -> String -> [Train] -> Int -> [Connection]
findConnections start end trains n = sortBy compareConnections notSortedConnections
    where
        -- Lista nieposortowanych połączeń.
        notSortedConnections = concat $ map (\(initD, initS) -> toNextStation (Connection [initS] initD) n trains) initInfo
        -- Lista pociągów odjeżdżających ze stacji początkowej.
        trains' = departingTrains start trains
        -- Lista krotek opisująca możliwe początkowe sekcje na trasach pociągów
        -- odjeżdżających ze stacji o danej nazwie. Każda krotka zawiera listę dni
        -- kursowania pociągu oraz sekcję początkową trasy.
        initInfo :: [( [Day], Section )]
        initInfo = map (\t -> (trDays t, getSection start t)) trains'
        -- Zwraca listę połączeń, które można utworzyć z danego połączenia poprzez
        -- dodanie nowej sekcji startującej z ostatniej trasy połączenia.
        toNextStation :: Connection -> Int -> [Train] -> [ Connection ]
        toNextStation conn@(Connection sections days') k tr
            -- Jeśli ostatnia sekcja reprezentuje dojazd do stacji docelowej, to
            -- zwracana jest lista zawierająca tylko połączenie wejściowe
            | seTo (last sections) == end = [conn]
            -- W p.p. generowane są połączenia wychodzące od danego połączenia spełniające warunki:
            | otherwise = concat [toNextStation (Connection (sections ++ [nextSec]) days'') k' tr |
                                  -- (lista krotek z nową sekcją, listą dni kursowania, liczbą przesiadek)
                                  (nextSec, days'', k') <- nextSections ((last sections), days', k) tr,
                                  -- istnieje przynajmniej jeden dzień, w którym kursują wszystkie pociągi
                                  commonDays days' days'' /= [], k' >= 0,
                                  -- aktualne połączenie nie zawiera nowej stacji
                                  isNewStation (seTo nextSec) conn,
                                  -- czas odjazdu z dotychczasowej ostatniej stacji do nowej stacji jest
                                  -- późniejszy niż czas przyjazdu na dotychczasową ostatnią stację
                                  later (seBegin nextSec) (seEnd (last sections))]

-- Tworzy listę następnych możliwych sekcji dostępnych z przekazanej sekcji.
-- Pierwszy argument zawiera aktualną sekcję, listę dni kursowania pociągów
-- na danym połączeniu oraz możliwą jeszcze liczbę przesiadek, w drugim argumencie
-- jest lista dostępnych pociągów. Każdy element zwracanej listy zawiera
-- nową sekcję, uaktualnioną listę dni oraz liczbę przesiadek.
nextSections :: (Section, [Day], Int) -> [Train] -> [ (Section, [Day], Int) ]
nextSections (section, days', n) trains = map (\t -> (getSection (seTo section) t, newDays t, nextN t) ) trains'
    where
        -- Lista pociągów odjeżdżających ze stacji docelowej danej sekcji
        trains' = departingTrains (seTo section) trains
        -- Zwraca nową listę dni, w których połączenie jest dostępne.
        newDays tr = commonDays days' (trDays tr)
        -- Zwraca nową liczbę możliwych przesiadek.
        nextN tr | trName tr == seTrain section = n
                 | otherwise                    = n - 1

-- Zwraca listę punktów trasy danego pociągu, która zaczyna się od
-- stacji o danej nazwie.
routeFrom :: String -> Train -> [RoutePoint]
routeFrom sname (Train _ _ route) = dropWhile notStartPoint (roStations route)
    where
        -- Funkcja sprawdzająca, czy dany punkt trasy nie jest związany
        -- ze stacją o przkazanej nazwie.
        notStartPoint = (/= sname).rpStation

-- Zwraca sekcję utworzoną dla danego pociągu reprezentującą odjazd
-- ze stacji o nazwie jak w pierwszym argumencie.
getSection :: String -> Train -> Section
getSection sname train = Section (trName train) sname to depT arrT
    where
        -- Trasa pociągu od danej stacji.
        rfrom = routeFrom sname train
        -- Dana stacja oraz następna stacja będąca na trasie pociągu.
        [rp1, rp2] = take 2 rfrom
        Just depT = rpDeparture rp1
        Just arrT = rpArrival rp2
        to = rpStation rp2

-- Zwraca listę pociągów spośród przekazanych pociągówi, które
-- odjeżdżają z danej stacji.
departingTrains :: String -> [Train] -> [Train]
departingTrains sname trains = filter (\t -> length (routeFrom sname t) >= 2) trains

-- Zwraca listę zawierającą dni występujące na obu przekazanych listach.
commonDays :: [Day] -> [Day] -> [Day]
commonDays days1 days2 = filter (\d -> d `elem` days2) days1

-- Sprawdza, czy stacja o danej nazwie nie występuje na danym połączeniu.
isNewStation :: String -> Connection -> Bool
isNewStation sname (Connection sections _) = all notInSection sections
    where
        notInSection (Section _ from to _ _) = sname /= from && sname /= to

-- Sprawdza, czy pierwszy czas jest późniejszy niż drugi czas.
later :: Time -> Time -> Bool
later (Time h m) (Time h' m') = if h > h' then True
                                else if h == h' && m > m' then True
                                     else False

