module Inout where

import Control.Exception.Base

-- Wczytanie z pliku listy zawierającej dane:
-- pociągi (trainz.txt) lub stacje (stationz.txt)
readDataFile :: (Read a) => FilePath -> IO [a]
readDataFile filename = do
        content <- readFile filename
        let elements = read content
        evaluate elements

-- Zapisanie do pliku listy zawierającej dane:
-- pociągi (trainz.txt) lub stacje (stationz.txt)
writeDataFile :: (Show a) => FilePath -> [a] -> IO ()
writeDataFile filename xs = do
        let content = show xs
        writeFile filename content
