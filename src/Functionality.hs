module Functionality where

import Data.Maybe
import Data.List
import System.Exit

import Types
import Inout

-- Operacja przekształcająca stan programu.
type Operation = [Train] -> [Station] -> IO ([Train], [Station])

-- Operacja dodania stacji. Zwraca stan z nową stacją.
newStation :: Operation
newStation trains stations = do
        newOne <- getStation stations
        return (trains, newOne : stations)

-- Operacja modyfikacji stacji. Zwraca stan z przemianowaną stacją oraz
-- pociągami o trasach uwzględniających zmianę nazwy.
modStation :: Operation
modStation trains stations = do
        putStrLn "Podaj nazwę stacji do modyfikacji:"
        name <- getLine
        if hasStation name stations then do
                let otherStations = filter (\x -> (stName x) /= name) stations
                modifiedStation <- getStation otherStations
                let newName = stName modifiedStation
                let trainsWithStationChanged = map (renameStationForTrain name newName) trains
                let stationWithRelatedTrains = foldl (\x y -> addTrainIfRelated y x) modifiedStation trainsWithStationChanged
                return (trainsWithStationChanged, stationWithRelatedTrains : otherStations)
                else do
                        putStrLn "Nie ma takiej stacji"
                        return (trains, stations)

-- Operacja usunięcia stacji. Zwraca stan z usuniętą stacją oraz pociągami
-- o trasach bez usuniętej stacji.
delStation :: Operation
delStation trains stations = do
        putStrLn "Podaj nazwę stacji do usunięcia:"
        name <- getLine
        if hasStation name stations then do
                let otherStations = filter (\x -> (stName x) /= name) stations
                let trainsWithStationRemoved = map (removeStationForTrain name) trains
                return (trainsWithStationRemoved, otherStations)
                else do
                        putStrLn "Nie ma takiej stacji"
                        return (trains, stations)

-- Operacja wyświetlenia stacji. Zwraca stan w postaci niezmienionej.
showStations :: Operation
showStations trains stations = do
        let showStation x = ((show . stName) x) ++ ", pociągi:"
                            ++ foldl (\y z -> y ++ " " ++ z) " " (stTrains x)
        _ <- mapM putStrLn (map showStation stations)
        return (trains, stations)

-- Operacja dodania pociągu. Zwraca stan z nowym pociągiem.
newTrain :: Operation
newTrain trains stations = do
        newOne <- getTrain stations trains
        return (newOne : trains, map (addTrainIfRelated newOne) stations)

-- Operacja modyfikacji pociągu. Zwraca stan ze zmodyfikowanym pociągiem oraz
-- stacjami uwzględniającymi przemianowanie pociągu.
modTrain :: Operation
modTrain trains stations = do
        putStrLn "Podaj nazwę pociągu do modyfikacji:"
        name <- getLine
        if hasTrain name trains then do
                let otherTrains = filter (\x -> (trName x) /= name) trains
                modifiedTrain <- getTrain stations otherTrains
                let stationsWithOldVersionRemoved = map (removeTrain name) stations
                let stationsWithTrainChanged = map (addTrainIfRelated modifiedTrain) stationsWithOldVersionRemoved
                return (modifiedTrain : otherTrains, stationsWithTrainChanged)
                else do
                        putStrLn "Nie ma takiego pociągu"
                        return (trains, stations)

-- Operacja modyfikacji pociągu. Zwraca stan z usuniętym pociągiem oraz
-- stacjami uwzględniającymi usunięcie pociągu.
delTrain :: Operation
delTrain trains stations = do
        putStrLn "Podaj nazwę pociągu do usunięcia:"
        name <- getLine
        if hasTrain name trains then do
                let otherTrains = filter (\x -> (trName x) /= name) trains
                let stationsWithoutRemovedTrain = map (removeTrain name) stations
                return (otherTrains, stationsWithoutRemovedTrain)
                else do
                        putStrLn "Nie ma takiego pociągu"
                        return (trains, stations)

-- Operacja wyświetlenia pociągu. Zwraca stan w postaci niezmienionej.
showTrains :: Operation
showTrains trains stations = do
        let showTrain x = ((show . trName) x) ++ ", stacje:"
                          ++ foldl (\y z -> y ++ " " ++ z) " " (map rpStation ((roStations . trRoute) x))
        _ <- mapM putStrLn (map showTrain trains)
        return (trains, stations)

-- Operacja wyświetlenia połączeń pomiędzy stacjami.
-- Pobiera stację początkową, końcową i maksymalną liczbę przesiadek,
-- a następnie wyświetla połączenia dla takich parametrów.
-- Zwraca stan w postaci niezmienionej.
findConnections :: Operation
findConnections trains stations = do
        putStrLn "Podaj nazwę stacji początkowej:"
        start <- getLine
        putStrLn "Podaj nazwę stacji końcowej:"
        end <- getLine
        putStrLn "Podaj maksymalną liczbę przesiadek:"
        nUnparsed <- getLine
        case parseNumber nUnparsed of
          Just n -> do
                let connections = Types.findConnections start end trains n
                putStrLn $ formatConnections connections start end n
          _      -> do
                putStrLn "Liczba przesiadek określona nieprawidłowo"
        return (trains, stations)

-- Operacja wyświetlenia rozkładu jazdy dla danej stacji. Zwraca stan w postaci
-- niezmienionej.
generateTimetable :: Operation
generateTimetable trains stations = do
        putStrLn "Podaj nazwę stacji, dla której ma być wygenerowany rozkład:"
        stationName <- getLine
        case findStation stationName stations of
          Just station -> do putStrLn $ formatTimeTable $ doTimeTable station trains
          _            -> do putStrLn "Nie ma takiej stacji"
        return (trains, stations)

-- Operacja zakończenia programu. Nic nie zwraca, ponieważ kończy program.
exit :: Operation
exit trains stations = do
        putStrLn "Zapisywanie stacji i pociągów do plików trainz.txt oraz stationz.txt"
        writeDataFile "trainz.txt" trains
        writeDataFile "stationz.txt" stations
        _ <- exitSuccess
        return (trains, stations)

-- Operacja obsługi nieznanego polecenia. Zwraca stan w postaci niezmienionej.
complain :: Operation
complain trains stations = do
        putStrLn "Nieznana operacja"
        return (trains, stations)

-- Pobiera od użytkownika stację o nazwie niekolidującej z innymi.
getStation :: [Station] -> IO Station
getStation stats = do
        let snames = map stName stats
        name <- getUniqueName snames "Podaj nazwę stacji:"
        return (Station name [])

-- Pobiera od użytkownika pociąg o nazwie niekolidującej z innymi
-- oraz o prawidłowej trasie.
getTrain :: [Station] -> [Train] -> IO Train
getTrain stats trains = do
        let tnames = map trName trains
        name <- getUniqueName tnames "Podaj nazwę pociągu:"
        days <- getDays
        route <- getRoute stats []
        return (Train name days route)

-- Wybiera od użytkownika nową, niekolidującą z innymi nazwę.
getUniqueName :: [String] -> String -> IO String
getUniqueName names msg = do
                            putStrLn msg
                            name <- getLine
                            let unique = all (/= name) names
                            case unique of
                                True -> if null name then do
                                          putStrLn "Nazwa jest pusta."
                                          getUniqueName names msg
                                          else do
                                               return name
                                False -> do
                                        putStrLn "Nazwa nie jest unikalna."
                                        putStr "Nazwy obecnie występujące(zajęte): "
                                        putStrLn $ (tail . init) $ show names
                                        getUniqueName names msg

-- Pobiera od użytkownika listę dni oddzielonych białymi znakami.
getDays :: IO [Day]
getDays = do
            putStrLn "Podaj dni kursowania pociągu (Po, Wt, Sr, Cz, Pt, So, Ni):"
            daysStr <- getLine
            let days = nub $ map parseDay $ words daysStr
            case Nothing `elem` days || null days of
              False -> return (catMaybes days)
              True  -> putStrLn "Nieprawidłowo zapisane dni, jeszcze raz:" >> getDays

-- Pobiera od użytkownika kolejne punkty trasy dla tworzonego pociągu, po czym zwraca
-- całą nową trasę.
getRoute :: [Station] -> [RoutePoint] -> IO Route
getRoute stations rps = do
                        let available = map stName stations
                        if null available then do
                          putStrLn "Nie ma więcej stacji"
                          return (Route rps)
                          else do
                                name <- getAvailableName available "Podaj nazwę stacji:"
                                let unusedStations = filter (\x -> name /= stName x) stations
                                answer <- if (not . null) unusedStations then do
                                            putStrLn "Czy to ostatnia stacja(t/n)?"
                                            getLine
                                            else do
                                              return ("t")
                                let isLast = case answer of "t" -> True
                                                            _   -> False
                                newRP <- getRoutePoint name rps isLast
                                if isLast then
                                  return (Route (rps ++ [newRP]))
                                  else
                                    getRoute unusedStations (rps ++ [newRP])

-- Pobiera od użytkownika jedną z dostępnych nazw stacji.
getAvailableName :: [String] -> String -> IO String
getAvailableName names msg = do
        putStrLn msg
        name <- getLine
        if name `elem` names then do
          return(name)
            else do
              putStrLn ("Stacja: " ++ name ++ " nie jest dostępna")
              getAvailableName names msg

-- Pobiera od użytkownika pojedynczy punkt trasy następujący po poprzednich.
getRoutePoint :: String -> [RoutePoint] -> Bool -> IO RoutePoint
getRoutePoint name rps isLast = do
        arrTime <- if null rps then do
                     return (Nothing)
                     else do
                       time <- getTime "Podaj godzinę przyjazdu (w formacie gg mm):"
                       return (Just time)
        depTime <- if isLast then do
                     return (Nothing)
                     else do
                       time <- getTime "Podaj godzinę odjazdu (w formacie gg mm):"
                       return (Just time)
        let got = RoutePoint name arrTime depTime
        if validRoutePoint got
           && validRoutePoints (rps ++ [got])
           && validWithOtherRoutePoints rps arrTime
           && validWithOtherRoutePoints rps depTime then do
          return got
          else do
            putStrLn "Niepoprawne czasy, jeszcze raz:"
            getRoutePoint name rps isLast

-- Pobiera od użytkownika czas w formacie: gg mm.
getTime :: String -> IO Time
getTime msg = do
                putStrLn msg
                strTime <- getLine
                case map parseNumber $ words strTime of
                    [Just hour, Just minutes] -> time
                            where time = if hour >= 0 && hour < 24 && minutes >= 0 && minutes < 60 then do
                                           return (Time hour minutes)
                                           else do
                                             putStrLn "Zły format czasu, jeszcze raz:" >> getTime msg
                    _ -> do putStrLn "Zły format czasu, jeszcze raz:" >> getTime msg

-- Zamienia napis na liczbę lub Nothing jeśli konwersja jest niemożliwa.
parseNumber :: String -> Maybe Int
parseNumber number = (fmap fst . listToMaybe . filter (null . snd) . reads) number

-- Zamienia napis na dzień tygodnia lub Nothing jeśli konwersja jest niemożliwa.
parseDay :: String -> Maybe Day
parseDay day = (fmap fst . listToMaybe . filter (null . snd) . reads) day
